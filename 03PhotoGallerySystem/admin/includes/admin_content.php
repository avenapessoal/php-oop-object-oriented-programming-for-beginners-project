<div id="page-wrapper">

	<div class="container-fluid">

		<!-- Page Heading -->
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">
					Admin Page <small>Subheading</small>
				</h1>
				<ol class="breadcrumb">
					<li><i class="fa fa-dashboard"></i> <a href="index.html">Dashboard</a>
					</li>
					<li class="active"><i class="fa fa-file"></i>Blank Page</li>
				</ol>
			</div>
				 <?php // if($database->connection) {echo "Conexao aberta"; } ?>
<?php
// aula 38 - codigo 2 na aula de desenvolvimento da classe user
// comentei na aula 046 - antes disso, deixei funcionando junto com outros codigos 
/* $result_set = User::find_all_users();
// pega do banco de dados e faz um array e imprimir um atributo do array (que veio do banco)
while ($row = mysqli_fetch_array($result_set)) {
    echo $row['username'] . "<br>";
}
 */
// comentei na aula 046 - antes disso, deixei funcionando junto com outros codigos
/* echo "<hr>";
$user_pass_id = 1;
$user_found = User::find_user_by_id($user_pass_id); */

// var_dump($user_found);
// imprimindo o valor via string - do banco de dados
// echo $user_found['username'];

/*
// instanciando o objeto user - aula 41
$user = new User();
// pegando valor da consulta string (do banco de dados) e enviando para o objeto usuario
$user->id = $user_found['id'];
$user->username = $user_found['username'];
$user->password = $user_found['password'];
$user->first_name = $user_found['first_name'];
$user->last_name = $user_found['last_name'];
// imprimindo o valor via objeto
echo "Meu nome: " . $user->first_name . " ". $user->last_name;

var_dump($user);
*/

// aula 43 - testando a funcao instantiation
/* $user = User::instantation($user_found);

echo $user->first_name;
echo '<br>'; */

/*
 * // aula 36-37 - codigo 1 na aula de desenvolvimento da classe user
 *
 * $user = new User();
 * $result_set = $user->find_all_users();
 *
 * while ($row = mysqli_fetch_array($result_set)) {
 * echo $row['username'] . "<br>";
 * }
 *
 */

/*
 * // codigo da aula da classe de conecao ao banco de dados
 * $sql = 'SELECT * FROM users WHERE id= 1';
 * $result = $database->query($sql);
 * //$user_found = mysqli_fetch_array($result);
 * $user_found = mysqli_fetch_assoc($result);
 *
 * echo $user_found['username'];
 *
 */
/*
 * // vendo toda estrutura do array
 * var_dump($user_found);
 *
 * // possivel ao transformar em um array associativo
 * foreach ($user_found as $v) {
 * echo $v . "<br/>";
 * }
 *
 * // possivel ao transformar em um array associativo, usando chave (k=key) e valor/v
 * echo "<br/>";
 * foreach ($user_found as $k => $v) {
 * echo $k . " => " . $v . "<br/>";
 * }
 *
 */


// 47 - using our instantiation method to find all user 
// funcao find_all_users da classe User este metodo estatico, que vai chamar a um funcao da classe Database 
// e fazer a query e retorna o resultado array, que na classe User este resultado vai ser interado/instantation
// e cada valor do array vai para um atributo do objeto User 
$users = User::find_all_users();
foreach ($users as $user){
    echo $user->username . "<br>";
}

// fim do 47 - somente este codigo faz a consulta, retorna, intera do array ao objeto, sendo entao impresso.

echo "<hr>";
// 48 - using our instantiation method to find 1 user 
$user_first = User::find_user_by_id(1);
echo $user_first->username;

// instancia um objeto de uma classe que nao existe para ver que o autoload (functions.php) da erro
//$pictures = new Pictures();


?>
		</div>
		<!-- /.row -->

	</div>
	<!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->