<?php

class Cars
{

    public $rodas = 4;

    public $portas = 4;

    function carDetalhe()
    {
        return "O carro tem $this->portas portas e $this->rodas rodas";
    }
    
    function setRodas(int $r):int{
        return $this->rodas = $r;
    }
} 

$palio = new Cars();
$palio->setRodas(8);
//$palio->setRodas("uma mensagem"); // da erro por ser string
echo $palio->carDetalhe();