<?php

class Carro
{

    public $rodas;

    function ola()
    {
        "Ola carro";
    }

    public function setRodas(int $r): int
    {
        return $this->rodas = $r;
    }
}

$bmw = new Carro();
$bmw->setRodas(4);

class Caminhao extends Carro
{
}

$truck = new Caminhao();
$truck->setRodas(8);


var_dump($bmw);
var_dump($truck);


