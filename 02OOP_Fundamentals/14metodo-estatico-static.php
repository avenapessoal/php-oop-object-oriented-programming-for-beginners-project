<?php

class Carro
{

    public $rodas;
    static $bancos = 4;

    function ola()
    {
        "Ola carro";
    }

    public function setRodas(int $r): int
    {
        return $this->rodas = $r;
    }
}

$bmw = new Carro();
$bmw->setRodas(4);
//$bmw->Carro::$bancos;

class Caminhao extends Carro
{
}

$truck = new Caminhao();
$truck->setRodas(8);


var_dump($bmw);
var_dump($truck);

// nao precisa de uma instancia
echo "Tem " . Carro::$bancos . " Bancos"; 
