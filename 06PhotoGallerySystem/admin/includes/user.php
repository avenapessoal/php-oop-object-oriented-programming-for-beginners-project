<?php

class User
{

    public $id;

    public $username;

    public $password;

    public $first_name;

    public $last_name;

    public static function find_all_users()
    {
        // aula 40 - cria o find_this_query - para reduzir codigo
        return self::find_this_query("SELECT * FROM users");
        
        /*
         * global $database;
         * $sql = "SELECT * FROM users";
         * $result_set = $database->query($sql);
         * return $result_set;
         */
    }

    public static function find_user_by_id($user_id)
    {
        global $database;
        /*
         * $result_set = self::find_this_query("SELECT * FROM users WHERE id = $user_id LIMIT 1");
         * $found_user = mysqli_fetch_array($result_set);
         */
        // aula 48 comentei acima, e escrevi novo codigo abaixo
        $the_result_array = self::find_this_query("SELECT * FROM users WHERE id = $user_id LIMIT 1");
        if (! empty($the_result_array)) {
            $first_item = array_shift($the_result_array);
            return $first_item;
        } else {
            return false;
        }
        // usando ternario seria assim, acima codigo sem ternario
        // return !empty($the_result_array) ? array_shift($the_result_array) : false;
        
              
        /*
         * global $database;
         * $sql = "SELECT * FROM users WHERE id = $user_id LIMIT 1";
         * $result_set = $database->query($sql);
         * $found_user = mysqli_fetch_array($result_set);
         * return $found_user;
         */
    }

    public static function find_this_query($sql)
    {
        global $database;
        $result = $database->query($sql);
        // incluida na aula 46 - onde ideia e focar no retorno de dados do banco para um objeto
        $the_object_array = array(); // cria um array vazio
                                     // faz um la�o que pega cada atributo/propriedades(username,last_name) do array do banco de dados
        while ($row = mysqli_fetch_array($result)) {
            // e cada atributo vai para o array do objeto (� um array ainda)
            $the_object_array[] = self::instantation($row);
        }
        return $the_object_array;
    }

    public static function instantation($the_record)
    {
        $the_object = new self();
        // pegando valor da consulta string (do banco de dados) e enviando para o objeto usuario
        
        // 042 - feito manualmente - cada campo do array para o objeto
        /*
         * $the_object->id = $user_found['id'];
         * $the_object->username = $user_found['username'];
         * $the_object->password = $user_found['password'];
         * $the_object->first_name = $user_found['first_name'];
         * $the_object->last_name = $user_found['last_name'];
         */
        // para cada registro (vindo do banco), tem um atributo [name] e valor (Fernando)
        foreach ($the_record as $the_attribute => $value) {
            // se objeto tiver o atributo vindo do registro, ...
            if ($the_object->has_the_atrribute($the_attribute)) {
                // coloca o dentro do objeto e seu atributo, o valor do atributo
                // isso se o retorno de has_the_atrribute for true
                $the_object->$the_attribute = $value;
            }
        }
        
        // 044 - short way auto instantiation
        // como automatizar cada campo do array para o objeto
        return $the_object;
    }

    // 045 - creating the attribute finder maethod
    public function has_the_atrribute($the_attribute)
    {
        // pegando os atributos/var/variaveis de um objeto
        $object_properties = get_object_vars($this);
        // retorna true, se dentro do array $object_properties
        // (pego a pouco do objeto), tem o atributo informado no $the_attribute
        return array_key_exists($the_attribute, $object_properties);
    }

    // 59 - creating the verify method part1
    // 60 - creating the verify method part2
    public static function verify_user($username,$password)
    {
        global $database;
        $username = $database->escape_string($username);
        $password = $database->escape_string($password);
        
        $sql = "SELECT * FROM users WHERE ";
        $sql .= "username = '{$username}' ";
        $sql .= "AND password = '{$password}' ";
        $sql .= "LIMIT 1";
        
        $the_result_array = self::find_this_query($sql);
        return ! empty($the_result_array) ? array_shift($the_result_array) : false;
    }
}