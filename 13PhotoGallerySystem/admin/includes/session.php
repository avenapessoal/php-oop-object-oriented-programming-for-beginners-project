<?php

// aula 53 criou parametros, construct e inicou/instanciou um objeto
class Session
{

    private $signed_in = false;

    public $user_id;
    
    public $count;

    public $message;

    public function is_signed_in()
    {
        return $this->signed_in;
    }

    // 055 - the login method
    // $user deve vir do banco de dados
    public function login($user)
    {
        if ($user) {
            // classe atual e sessao atual - classe User->id (parametro da User)
            $this->user_id = $_SESSION['user_id'] = $user->id;
            $this->signed_in = true;
        }
    }

    // 056 - the logout method
    public function logout($user)
    {
        unset($this->user_id);
        unset($_SESSION['user_id']);
        $this->signed_in = false;
    }

    function __construct()
    {
        session_start();
        $this->visitor_count();
        $this->check_the_login();
        $this->check_message();
    }
    


    // 167 - Tracking Page Views
    public function visitor_count()
    {
        if (isset($_SESSION['count'])) {
            return $this->count = $_SESSION['count'] ++;
        } else {
            return $_SESSION['count'] = 1;
        }
    }
    
    
    //205 - Creating Session Methods
    public function message($msg = "")
    {
        if (! empty($msg)) {
            $_SESSION['message'] = $msg;
        } else {
            return $this->message;
        }
    }

    //205 - Creating Session Methods
    private function check_message()
    {
        if (isset($_SESSION['message'])) {
            $this->message = $_SESSION['message'];
            unset($_SESSION['message']);
        } else {
            $this->message = "";
        }
    }

    private function check_the_login()
    {
        if (isset($_SESSION['user_id'])) {
            $this->user_id = $_SESSION['user_id'];
            $this->signed_in = true;
        } else {
            unset($this->user_id);
            $this->signed_in = false;
        }
    }
}

$session = new Session();
//205 - Creating Session Methods
// acima instancia o objeto, e abaixo ja atribui o valor a variavel $message
$message = $session->message();

?>