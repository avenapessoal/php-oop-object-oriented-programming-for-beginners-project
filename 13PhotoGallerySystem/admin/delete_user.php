<?php include("includes/init.php"); ?>

<?php

// 135 - Deletion Users - copiou o delete_photo e trocou o nome.
// e funcao delete, a delete_photo - remove tambem o arquivo no sistema
if (! $session->is_signed_in()) {
    redirect("login.php");
}

?>

<?php

if (empty($_GET['id'])) {
    redirect("users.php");
}

$user = user::find_by_id($_GET['id']);

if ($user) {
    //$user->delete();
    // 207 
    $user->delete_photo();
    $session->message("Usuario {$user->username} foi deletado");
    redirect("users.php");
} else {
    redirect("users.php");
}

?>