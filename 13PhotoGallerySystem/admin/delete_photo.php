<?php include("includes/init.php"); ?>

<?php

// 063 - logim form creation
if (! $session->is_signed_in()) {
    redirect("login.php");
}

?>

<?php

if (empty($_GET['id'])) {
    redirect("photos.php");
}

$photo = Photo::find_by_id($_GET['id']);

if ($photo) {
    
    $photo->delete_photo();
    $session->message("Foto {$photo->filename} foi deletado");
    redirect("photos.php");
} else {
    redirect("photos.php");
}

?>