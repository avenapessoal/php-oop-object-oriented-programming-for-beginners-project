<?php include("includes/header.php"); ?>

<?php

// 063 - logim form creation
if (! $session->is_signed_in()) {
    redirect("login.php");
}

?>

<?php

$message = "";
/*
 * comentado na aula 212 - pois vamos usar o dropzone
 * // 111 - Uploading and Testing
 * if (isset($_POST['submit'])) {
 * $photo = new Photo();
 * $photo->title = $_POST['title'];
 * $photo->set_file($_FILES['file_upload']);
 *
 * if ($photo->save()) {
 * $message = "Foto enviado com sucesso!";
 * } else {
 * $message = join("<br>", $photo->errors);
 * }
 * }
 */
// 212
if (isset($_FILES['file'])) {
    $photo = new Photo();
    $photo->title = $_POST['title'];
    $photo->set_file($_FILES['file']);
    
    if ($photo->save()) {
        $message = "Foto enviado com sucesso!";
    } else {
        $message = join("<br>", $photo->errors);
    }
}

?>

<!-- Navigation -->
<!-- Brand and toggle get grouped for better mobile display -->

<?php include "includes/top_nav.php";?>

<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

<?php include 'includes/side_nav.php';?>
<!-- /.navbar-collapse -->

<div id="page-wrapper">

	<div class="container-fluid">

		<!-- Page Heading -->
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Upload Page</h1>

				<!-- 211 row -->
				<div class="row">
					<!-- 110 html form creation -->
					<div class="col-md-6">
					<?php echo $message; ?>
					<form action="upload.php" method="post"
							enctype="multipart/form-data">
							<div class="form-group">
								<input type="text" name="title" class="form-control" />
							</div>


							<div class="form-group">
								<input type="file" name="file" />
							</div>

							<input type="submit" name="submit" value="Enviar" />


						</form>

					</div>
				</div>
				<!-- end 211 row -->
				<div class="row">
					<!-- 211 row -->
					<div class="col-lg-12">

						<form action="upload.php" class="dropzone"></form>


					</div>
				</div>
			</div>
		</div>
		<!-- /.row -->

	</div>
	<!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->



<?php include("includes/footer.php"); ?>