<?php include("includes/header.php"); ?>

<?php

// 063 - logim form creation
if (! $session->is_signed_in()) {
    redirect("login.php");
}

?>

<?php
// 136 - Creating Add User Page - Part 1
// $user = user::find_by_id($_GET['id']);
$user = new User();

if (isset($_POST['create'])) {
    
    if ($user) {
        $user->username = $_POST['username'];
        $user->first_name = $_POST['first_name'];
        $user->last_name = $_POST['last_name'];
        $user->password = $_POST['password'];
        
        if (empty($_FILES['user_image'])) {
            $user->save();
        } else {
            $user->set_file($_FILES['user_image']);
            $user->upload_photo();
            // 209
            $session->message("O usuario {$user->username} foi criado");
            $user->save();
            redirect("users.php?id={$user->id}");
        }
        
        /*
         * $user->set_file($_FILES['user_image']);
         *
         * $user->save_user_and_image();
         *
         * redirect('users.php');
         */
    }
}

?>

<!-- Navigation -->
<!-- Brand and toggle get grouped for better mobile display -->

<?php include "includes/top_nav.php";?>

<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

<?php include 'includes/side_nav.php';?>
<!-- /.navbar-collapse -->

<div id="page-wrapper">

	<div class="container-fluid">

		<!-- Page Heading -->
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Adicionar User</h1>
				<!-- form em 123 - creating the edit page 2  -->
				<form action="" method="post" enctype="multipart/form-data">

					<!-- 112 Coding HTML for our users Table  -->
					<div class="col-md-6 col-md-offset-3">

						<div class="form-group">
							<input type="file" name="user_image" />
						</div>




						<div class="form-group">
							<label for="username">Username</label> <input type="text"
								name="username" class="form-control" />
						</div>


						<div class="form-group">
							<label for="first_name">First Name</label> <input type="text"
								name="first_name" class="form-control" />
						</div>

						<div class="form-group">
							<label for="last_name">Last Name</label> <input type="text"
								name="last_name" class="form-control" />
						</div>

						<div class="form-group">
							<label for="password">Password</label> <input type="text"
								name="password" class="form-control" />
						</div>

						<input type="submit" name="create" value="Registrar Usuario"
							class="btn btn-primary pull-right" />

					</div>
				</form>
			</div>
		</div>
		<!-- /.row -->

	</div>
	<!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->



<?php include("includes/footer.php"); ?>