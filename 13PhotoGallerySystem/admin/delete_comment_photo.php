<?php include("includes/init.php"); ?>

<?php

// 135 - Deletion comments - copiou o delete_photo e trocou o nome.
// e funcao delete, a delete_photo - remove tambem o arquivo no sistema
if (! $session->is_signed_in()) {
    redirect("login.php");
}

?>

<?php

if (empty($_GET['id'])) {
    redirect("comments.php");
}

$comment = Comment::find_by_id($_GET['id']);

if ($comment) {
    $comment->delete();
    redirect("comment_photo.php?id={$comment->photo_id}");
} else {
    redirect("comment_photo.php?id={$comment->photo_id}");
}

?>