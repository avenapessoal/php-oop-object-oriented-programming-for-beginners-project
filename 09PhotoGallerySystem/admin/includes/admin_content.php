<div id="page-wrapper">

	<div class="container-fluid">

		<!-- Page Heading -->
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">
					Admin Page <small>Subheading</small>
				</h1>
				
				<?php
    // 80 - Testing our Method Solution - create user
    /*
     * $user = new User();
     * $user->username = "iza";
     * $user->password = "123";
     * $user->first_name = "Izabella";
     * $user->last_name = "Victoriano";
     *
     * $user->create();
     */
    
    // 82 - testing our UPDATE method
    /*
     * $user = User::find_user_by_id(1);
     * $user->last_name = "Padilha";
     * $user->update();
     * $user = User::find_user_by_id(3);
     * $user->last_name = "Delicia";
     * $user->update();
     */
    
    // 83 - Delete Method 084- testing the delete method
    // $user = User::find_user_by_id(5);
    // $user->delete();
    
    // 86 - Improving the Create Method
    // atualizando o id existente
    // $user = USER::find_user_by_id(4);
    // $user->first_name = 'Izabella';
    // $user->save();
    // criando novo usuario
    // $user = new User();
    // $user->username = 'Suave';
    // $user->save();
    
    // 092 - testing the abstracted create method
    // $user = new User();
    // $user->username = 'anamorena';
    // $user->password = '123';
    // $user->first_name = "Ana";
    // $user->last_name = "Morena";
    // $user->create();
    
    // 94 testing de abstracted update
    // $user = User::find_user_by_id(4);
    // $user->password = "123";
    // $user->last_name = "Delicia";
    // $user->update();
    
    // 95 - Escaping Values From our Abstracted Methods
    $user = new User();
    $user->username = 'izadelicia';
    $user->password = '123';
    $user->first_name = "Iza";
    $user->last_name = "Deliciosa";
    $user->create();
    ?>
				
				<ol class="breadcrumb">
					<li><i class="fa fa-dashboard"></i> <a href="index.html">Dashboard</a>
					</li>
					<li class="active"><i class="fa fa-file"></i>Blank Page</li>
				</ol>
			</div>
				 <?php // if($database->connection) {echo "Conexao aberta"; } ?>
<?php
$user_first = User::find_user_by_id(2);
echo $user_first->username;

?>
		</div>
		<!-- /.row -->

	</div>
	<!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->