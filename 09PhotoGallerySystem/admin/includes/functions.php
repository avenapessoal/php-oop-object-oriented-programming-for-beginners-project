<?php

// http://php.net/manual/pt_BR/language.oop5.autoload.php
// Embora a fun��o __autoload() tamb�m pode ser utilizada para carregar automaticamente classes e interfaces,
// � prefer�vel a utiliza��o da fun��o spl_autoload_register().
/*
 * function __autoload($class)
 * {
 * $class = strtolower($class);
 * $the_path = "includes/{$class}.php";
 *
 * if (file_exists($the_path)) {
 * require_once ($the_path);
 * } else {
 *
 * die("<br/>This file name {$class}.php was not found man....");
 * }
 * }
 */
/*
 * function classAutoLoader($class)
 * {
 * $class = strtolower($class);
 * $the_path = "includes/{$class}.php";
 *
 * if (file_exists($the_path)) {
 * require_once ($the_path);
 * } else {
 *
 * die("<br/>This file name {$class}.php was not found man....");
 * }
 * }
 */
function classAutoLoader($class)
{
    $class = strtolower($class);
    $the_path = "includes/{$class}.php";
    
    if (is_file($the_path) && ! class_exists($class)) {
        require_once ($the_path);
    } else {
        
        die("<br/>This file name {$class}.php was not found man....");
    }
}

spl_autoload_register('classAutoLoader');


// 057 - controlling access to admin
function redirect($location)
{
    header("Location: {$location}");
}



?>