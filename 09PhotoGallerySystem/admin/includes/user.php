<?php

class User
{

    // 87 - abstracting tables self::$db_tables
    protected static $db_tables = "users";

    protected static $db_table_fields = array(
        'username',
        'password',
        'first_name',
        'last_name'
    );

    public $id;

    public $username;

    public $password;

    public $first_name;

    public $last_name;

    public static function find_all_users()
    {
        // aula 40 - cria o find_this_query - para reduzir codigo
        return self::find_this_query("SELECT * FROM users");
        
        /*
         * global $database;
         * $sql = "SELECT * FROM users";
         * $result_set = $database->query($sql);
         * return $result_set;
         */
    }

    public static function find_user_by_id($user_id)
    {
        global $database;
        /*
         * $result_set = self::find_this_query("SELECT * FROM users WHERE id = $user_id LIMIT 1");
         * $found_user = mysqli_fetch_array($result_set);
         */
        // aula 48 comentei acima, e escrevi novo codigo abaixo
        $the_result_array = self::find_this_query("SELECT * FROM users WHERE id = $user_id LIMIT 1");
        if (! empty($the_result_array)) {
            $first_item = array_shift($the_result_array);
            return $first_item;
        } else {
            return false;
        }
        // usando ternario seria assim, acima codigo sem ternario
        // return !empty($the_result_array) ? array_shift($the_result_array) : false;
        
        /*
         * global $database;
         * $sql = "SELECT * FROM users WHERE id = $user_id LIMIT 1";
         * $result_set = $database->query($sql);
         * $found_user = mysqli_fetch_array($result_set);
         * return $found_user;
         */
    }

    public static function find_this_query($sql)
    {
        global $database;
        $result = $database->query($sql);
        // incluida na aula 46 - onde ideia e focar no retorno de dados do banco para um objeto
        $the_object_array = array(); // cria um array vazio
                                     // faz um la�o que pega cada atributo/propriedades(username,last_name) do array do banco de dados
        while ($row = mysqli_fetch_array($result)) {
            // e cada atributo vai para o array do objeto (� um array ainda)
            $the_object_array[] = self::instantation($row);
        }
        return $the_object_array;
    }

    public static function instantation($the_record)
    {
        $the_object = new self();
        // pegando valor da consulta string (do banco de dados) e enviando para o objeto usuario
        
        // 042 - feito manualmente - cada campo do array para o objeto
        /*
         * $the_object->id = $user_found['id'];
         * $the_object->username = $user_found['username'];
         * $the_object->password = $user_found['password'];
         * $the_object->first_name = $user_found['first_name'];
         * $the_object->last_name = $user_found['last_name'];
         */
        // para cada registro (vindo do banco), tem um atributo [name] e valor (Fernando)
        foreach ($the_record as $the_attribute => $value) {
            // se objeto tiver o atributo vindo do registro, ...
            if ($the_object->has_the_atrribute($the_attribute)) {
                // coloca o dentro do objeto e seu atributo, o valor do atributo
                // isso se o retorno de has_the_atrribute for true
                $the_object->$the_attribute = $value;
            }
        }
        
        // 044 - short way auto instantiation
        // como automatizar cada campo do array para o objeto
        return $the_object;
    }

    // 045 - creating the attribute finder maethod
    public function has_the_atrribute($the_attribute)
    {
        // pegando os atributos/var/variaveis de um objeto
        $object_properties = get_object_vars($this);
        // retorna true, se dentro do array $object_properties
        // (pego a pouco do objeto), tem o atributo informado no $the_attribute
        return array_key_exists($the_attribute, $object_properties);
    }

    // 59 - creating the verify method part1
    // 60 - creating the verify method part2
    public static function verify_user($username, $password)
    {
        global $database;
        $username = $database->escape_string($username);
        $password = $database->escape_string($password);
        
        $sql = "SELECT * FROM users WHERE ";
        $sql .= "username = '{$username}' ";
        $sql .= "AND password = '{$password}' ";
        $sql .= "LIMIT 1";
        
        $the_result_array = self::find_this_query($sql);
        return ! empty($the_result_array) ? array_shift($the_result_array) : false;
    }

    // 88 - abstracting properties -> 89 - abstracting the create method - part 1
    protected function properties()
    {
        // return get_object_vars($this);
        // 91 - modifying the properties method, pois
        // $db_tables = "users" e $id - nao poderia ser usado em alguns sql
        // entao criou o $db_table_fields - somente com campos que precisa
        // inicia a variavel como um array
        $properties = array();
        
        foreach (self::$db_table_fields as $db_field) {
            // se objeto ou classe e uma propriedade - $db_field nao propriedade
            if (property_exists($this, $db_field))
                $properties[$db_field] = $this->$db_field;
        }
        return $properties;
    }

    // 95 - Escaping Values From our Abstracted Methods
    protected function clean_properties()
    {
        global $database;
        $clean_properties = array();
        foreach ($this->properties() as $key => $value) {
            $clean_properties[$key] = $database->escape_string($value);
        }
        return $clean_properties;
    }

    // 077 - Create Methodo Query part1 - aula de crud - create
    public function create()
    {
        global $database;
        
        // 88 - abstracting properties -> 89 - abstracting the create method - part 1
        // $properties = $this->properties();
        // 95 - Escaping Values From our Abstracted Methods
        $properties = $this->clean_properties();
        
        // $sql = "INSERT INTO ". self::$db_tables ." (username, password, first_name,last_name)";
        // 89 - abstracting the create method - part 1 - implode e pega as chaves do array - como acima
        $sql = "INSERT INTO " . self::$db_tables . "(" . implode(",", array_keys($properties)) . ")";
        // 90 - abstracting the create method - part 2
        $sql .= "VALUES ('" . implode("','", array_values($properties)) . "')";
        
        /*
         * $sql .= $database->escape_string($this->username) . "','";
         * $sql .= $database->escape_string($this->password) . "','";
         * $sql .= $database->escape_string($this->first_name) . "','";
         * $sql .= $database->escape_string($this->last_name) . "')";
         */
        
        if ($database->query($sql)) {
            $this->id = $database->the_insert_id();
            return true;
        } else {
            return false;
        }
    }

    // 86 - Improving the Create Method
    public function save()
    {
        return isset($this->id) ? $this->update() : $this->create();
    }

    // 81 - update method query
    // vai atualizar dados do usuario ja existente
    public function update()
    {
        global $database;
        
        // 93 abstracting the UPDATE method
        // $properties = $this->properties();
        // 95 - Escaping Values From our Abstracted Methods
        $properties = $this->clean_properties();
        $properties_pairs = array();
        
        foreach ($properties as $key => $value) {
            $properties_pairs[] = "{$key}='{$value}'";
        }
        
        $sql = "UPDATE " . self::$db_tables . "  SET ";
        // 93 abstracting the UPDATE method - o foreach vai criar o que foi criado abaixo e esta comentado
        $sql .= implode(", ", $properties_pairs);
        $sql .= " WHERE id = " . $database->escape_string($this->id);
        
        // $sql .= "username = '" . $database->escape_string($this->username) . "', ";
        // $sql .= "password = '" . $database->escape_string($this->password) . "', ";
        // $sql .= "first_name = '" . $database->escape_string($this->first_name) . "', ";
        // $sql .= "last_name = '" . $database->escape_string($this->last_name) . "' ";
        // $sql .= " WHERE id = " . $database->escape_string($this->id);
        
        $database->query($sql);
        
        return (mysqli_affected_rows($database->connection) == 1) ? true : false;
    }

    public function delete()
    {
        global $database;
        
        $sql = "DELETE FROM  " . self::$db_tables;
        $sql .= " WHERE id = " . $database->escape_string($this->id);
        // funcao de deletar limitado somente a um registro. SEGURAN�A
        $sql .= " LIMIT 1";
        
        $database->query($sql);
        
        return (mysqli_affected_rows($database->connection) == 1) ? true : false;
    }
} // fim da classe user






